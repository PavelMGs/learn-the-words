import { CLEAR } from './actionTypes';

export const clearAction = () => ({
    type: CLEAR,
})