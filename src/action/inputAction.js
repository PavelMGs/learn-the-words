import { PRINT } from "./actionTypes"

export const inputAction = (letter) => ({
        type: PRINT,
        inputPayload: letter,
});

