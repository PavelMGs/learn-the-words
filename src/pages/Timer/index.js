import React, { Component } from 'react'
import { Layout, Typography, Button } from 'antd';
import Clock from '../../components/Clock';

import s from './Timer.module.scss';

const { Content } = Layout;
const { Title } = Typography;

class TimerPage extends Component {
    state = {
        showTimer: true
    }
    
    handleToogleClock = () => {
        this.setState(({showTimer}) => {
            return {
                showTimer: !showTimer,
            }
        })
    }

    render() {
        const { showTimer } = this.state;

        return (
            <Layout>
                <Content>
                    <div className={s.root}>
                        <Title>Часики тик-так</Title>
                        <Button
                        type="primary"
                        onClick={this.handleToogleClock}
                        >    
                            Вкл./Выкл.
                        </Button>
                        { showTimer && <Clock currentDate ={new Date()} />}
                    </div>
                </Content>
            </Layout>
        )
    }
}

export default TimerPage;
