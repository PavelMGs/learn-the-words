import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  ClockCircleOutlined,
  HomeOutlined,
  SmileOutlined,
} from "@ant-design/icons";

import BackgroundBlock from "../../components/BackgroundBlock";
import CardList from "../../components/CardList";
import Footer from "../../components/Footer";
import Header from "../../components/Header";
import Paragraph from "../../components/Paragraph";
import Section from "../../components/Section";
import { Input, Button, Spin } from 'antd';
import getTranslateWord from '../../services/dictionary';

import firstBackground from "../../assets/background.jpg";
import secondBackground from "../../assets/back2.jpg";

import s from "./Home.module.scss";
import FirebaseContext from '../../context/FirebaseContext';
import { bindActionCreators } from 'redux';
import { cardListRejectAction, cardListResolveAction, fetchCardList } from '../../action/cardListAction';

const uid = localStorage.getItem('user');


class HomePage extends Component {
  state = {
    wordArr: [],
    isBusy: false,
    label: 'Проверьте правильность написания слова',
    hide_label: true
  };
  urlRequest = `/${uid}`;


  componentDidMount() {
    const { database } = this.context;
    const {
      fetchCardList
    } = this.props;

    fetchCardList(database, this.urlRequest)

  };

  engInputRef = React.createRef();

  handleDeletedItem = (id) => {
    const { items } = this.props;
    const { database } = this.context;

    const newWordArr = items.filter(item => item.id !== id)
    database.ref(this.urlRequest).set(newWordArr);
    this.props.fetchCardListResolve(newWordArr);
  };

  handleEngChange = (e) => {
    this.setState({
      eng: e.target.value
    })
  }


  getTheWord = async () => {

    const { database } = this.context;
    const { items } = this.props;

    getTranslateWord(this.state.eng)
      .then((getWord) => {
        let newWordArr = [];
        newWordArr = items.concat({
          id: Date.now(),
          eng: this.state.eng,
          rus: getWord.translate,
          isRemembered: false,
        });

        this.setState({
          ...this.state,
          eng: '',
          isBusy: true,
        })
        database.ref(this.urlRequest).set(newWordArr)
          .then(res => {
            this.setState({
              ...this.state,
              isBusy: false,
            });
            this.props.fetchCardListResolve(newWordArr)
            this.engInputRef.current.focus()
          })
      })
      .catch((err) => {
        this.setState({
          ...this.state,
          isBusy: false,
          hide_label: false,
        });

        setTimeout(() => {
          this.setState({
            ...this.state,
            hide_label: true,
          });
        }, 3000)
      })
  }

  handlePostRemember = (id, isRemembered) => {
    let sendArr = this.props.items;
    sendArr = sendArr.map((item) => {
      if(item.id === id) {
        item.isRemembered = !isRemembered;
      }
      console.log(id, item.id)
      return item;
    });
    console.log(sendArr)
    this.context.database.ref(this.urlRequest).set(sendArr);
    this.props.fetchCardListResolve(sendArr);
  };

  render() {
    const {
      items,
    } = this.props;
    const { isBusy, label, hide_label } = this.state;
    if (isBusy) {
      return <Spin />
    }
    return (
      <>
        <BackgroundBlock backgroundImg={firstBackground} fullHeight>
          <Header white>
            Время учить слова онлайн
            </Header>
          <Paragraph white>
            Используйте карточки для запоминания и пополняйте активный словарный
            запас.
            </Paragraph>
          <button
            className={s.bigButton}
            onClick={(engInputRef) => {
              this.engInputRef.current.focus();
            }}
          >
            Начать бесплатный урок
          </button>
        </BackgroundBlock>
        <Section className={s.textCenter}>
          <Header size="l">
            Мы создали уроки, чтобы помочь вам увереннее разговаривать на
            английском языке
            </Header>
          <div className={s.motivation}>
            <div className={s.motivationBlock}>
              <div className={s.icons}>
                <ClockCircleOutlined />
              </div>
              <Paragraph small>Учитесь, когда есть свободная минутка</Paragraph>
            </div>

            <div className={s.motivationBlock}>
              <div className={s.icons}>
                <HomeOutlined />
              </div>
              <Paragraph small>
                Откуда угодно — дома, в&nbsp;офисе, в&nbsp;кафе
                </Paragraph>
            </div>

            <div className={s.motivationBlock}>
              <div className={s.icons}>
                <SmileOutlined />
              </div>
              <Paragraph small>
                Разговоры по-английски без&nbsp;неловких пауз
                и&nbsp;«mmm,&nbsp;how&nbsp;to&nbsp;say…»
                </Paragraph>
            </div>
          </div>
        </Section>
        <Section bgColor="#f0f0f0" className={s.textCenter}>
          <Header size="l">Начать учить английский просто</Header>
          <Paragraph>
            Кликай по карточкам и узнавай новые слова, быстро и легко!
            </Paragraph>

          <div className={s.proposition}>
            <div>Введите через пробел слово на английском и на</div>
            <div>русском, чтобы добавить карточку</div>
          </div>

          {
            <div className={hide_label ? s.label_hidden : s.label}>
              { label }
            </div>
          }

          <div className={s.form}>
            <div className={s.inputWords}>
              <Input
                ref={this.engInputRef}
                placeholder='Слово на английском'
                onChange={this.handleEngChange}
                onPressEnter={this.getTheWord}
                value={this.state.eng}
              />
              <Button
                type='primary'
                onClick={this.getTheWord}
              >
                Добавить
                </Button>
            </div>
          </div>

          {
            isBusy
              ? <Spin />
              : <CardList
                onDeletedItem={this.handleDeletedItem}
                items={items}
                handlePostRemember={this.handlePostRemember}
              />
          }
        </Section>
        <BackgroundBlock backgroundImg={secondBackground}>
          <Header size="l" white>
            Изучайте английский с персональным сайтом помощником
            </Header>
          <Paragraph white>Начните прямо сейчас</Paragraph>
        </BackgroundBlock>
        <Footer />
      </>
    );
  }
}

HomePage.contextType = FirebaseContext;

const mapStateToProps = (state) => {
  return {
    isBusy: state.cardList.isBusy,
    items: state.cardList.payload || [],
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    dispatch,
    fetchCardList: fetchCardList,
    fetchCardListResolve: cardListResolveAction,
    fetchCardListReject: cardListRejectAction,
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);