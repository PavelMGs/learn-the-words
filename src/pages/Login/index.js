import React, { Component } from "react";
import { Layout, Form, Input, Button } from "antd";

import s from "./Login.module.scss";
import { withFirebase } from "../../context/FirebaseContext";
import Registration from "../Registration";
const { Content } = Layout;

class LoginPage extends Component {
  state = {
    location: 'login'
  }

  onFinish = ({email, password}) => {
    const { signWithEmail, setUserUid } = this.props.firebase;
    const { history } = this.props;

    signWithEmail(email, password)
      .then(res => {
        setUserUid(res.user.uid);
        localStorage.setItem('user', res.user.uid);
        history.push('/');
      });
  };

  onFinishFailed = (errorMsg) => {
    console.log("####: errorMsg", errorMsg);
  };

  renderForm = () => {
    const layout = {
        labelCol: { span: 6 },
        wrapperCol: { span: 18 },
      };
      const tailLayout = {
        wrapperCol: { offset: 6, span: 18 },
      };

      return (
          
        <Form
        {...layout}
        name="basic"
        initialValues={{ remember: true }}
        onFinish={this.onFinish}
        onFinishFailed={this.onFinishFailed}
      >
        <Form.Item
          label="Username"
          name="email"
          rules={[
            { required: true, message: "Please input your email!" },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[
            { required: true, message: "Please input your password!" },
          ]}
        >
          <Input.Password />
        </Form.Item>


        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
        <div className={s.registration_block}>
        <span>
          Do not have an account?
        </span>
          <Form.Item className={s.registration_button}>
            <Button type="primary" onClick={() => this.setState({
              ...this.state,
              location: 'registration',
            })}>
              Registration
            </Button>
          </Form.Item>
        </div>
      </Form>
      )
  }

  render() {
    const { location } = this.state; 
    return (
      <Layout>
        <Content>
          <div className={s.root}>
            <div className={s.form_wrap}>
                { 
                  location === 'registration'
                  ? <Registration />
                  : this.renderForm()
                }
            </div>
          </div>
        </Content>
      </Layout>
    );
  }
}

export default withFirebase(LoginPage);
