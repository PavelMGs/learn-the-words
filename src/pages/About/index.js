import React, { Component } from 'react';
import BackgroundBlock from '../../components/BackgroundBlock';
import firstBackground from '../../assets/background.jpg'
import s from './About.module.scss';

class About extends Component{
    render() {
        return (
            <div className={s.root}>
                <BackgroundBlock backgroundImg={firstBackground} fullHeight>
                    <div className={s.content}>
                        <article>
                            Наш сайт поможет тебе в изучении английских слов. Просто напиши в поле ввода слово на английском, чтобы добавить его карточку с переводом в список. Слова можно помечать как изученные, а также удалять после изучения.  
                        </article>
                        <span>
                            Разработано в рамках React-марафона под руководством Zar Zakharov.
                        </span>
                    </div>

                </BackgroundBlock>
            </div>
        )
    }
}

export default About;