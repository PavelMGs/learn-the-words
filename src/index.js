import React from 'react';
import ReactDom from 'react-dom';

import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import rootReducers from './reducers/index';
import App from './App';
import 'antd/dist/antd.css';
import './index.css';
import FirebaseContext from './context/FirebaseContext';
import Firebase from './services/firebase';
import { BrowserRouter } from 'react-router-dom';
import thunk from 'redux-thunk';

const store = new createStore(rootReducers, applyMiddleware(thunk));

ReactDom.render(
<Provider store={store}>
    <FirebaseContext.Provider value={new Firebase()}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </FirebaseContext.Provider>
</Provider>
, document.getElementById('root'));