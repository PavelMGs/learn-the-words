/* eslint-disable no-unused-expressions */
import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export const PrivateRoute = ({component: Component, ...rest}) => (
    <Route 
        {...rest}
        render={ props => console.log(props.location)
                ? <Component {...props} />
                : localStorage.getItem('user') 
                    ? <Component {...props} /> 
                    : <Redirect to="/login" />}
    />
)