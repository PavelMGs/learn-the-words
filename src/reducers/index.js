import { combineReducers } from 'redux';
import cardListReducer from './cardListReducer';
import userReducer from './userReducer';

export default combineReducers({
    user: userReducer,
    cardList: cardListReducer,
});