import { CLEAR, PRINT } from '../action/actionTypes'

const inputReducer = (state = { engInput: ' ' }, action) => {
    switch(action.type) {
        case PRINT: 
            return {
                ...state,
                engInput: action.inputPayload,
            }
        case CLEAR:
            return {
                ...state,
                engInput: " ",
            }
        default:
            console.log('1');
            return state;
    }

};

export default inputReducer;