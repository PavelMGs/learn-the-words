import React from 'react'
import s from './FooterBlock.module.css'

const FooterBlock = ({ title }) => {
    return (
        <footer className={s.footer}>
            {title}
        </footer>
    )
}

export default FooterBlock;