import React, { Component } from 'react'
import Title from 'antd/lib/skeleton/Title';

class Clock extends Component {
    state = {
        date: this.props.currentDate,
    };

    constructor(props) {
        super(props);
        console.log();
    }

    componentDidMount() {
        this.interval = setInterval(this.tick, 1000)
    };

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state.date < nextState.date) {
            return false;
        }
        else { 
            return true;
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state !== prevState) {
            this.setState();
        }

    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    tick = () => {
        this.setState({
            date: new Date()
        });
    };


    render() {
        const { date } = this.state;
        return (
            <>
                <Title level={2}>
                    Сейчас {date.toLocaleTimeString()}
                </Title>   
            </>
        );
    };
};

export default Clock;