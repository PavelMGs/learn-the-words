import React from 'react';
import s from './Footer.module.scss';

const Footer = () => {
    return (
        <footer className={s.root}>
            ©PavelMGs 2020
        </footer>
    );
};

export default Footer;