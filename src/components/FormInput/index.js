import React, { useState, Component } from 'react';
import { Form, Input, Button, Radio } from 'antd';
import s from './FormInput.module.scss';

const FormInput  = () => {
  const [form] = Form.useForm();
  const [formLayout, setFormLayout] = useState('inline');

  const onFormLayoutChange = ({ layout }) => {
    setFormLayout(layout);
  };

  const formItemLayout =
    formLayout === 'horizontal'
      ? {
          labelCol: { span: 4 },
          wrapperCol: { span: 14 },
        }
      : null;

  const buttonItemLayout =
    formLayout === 'horizontal'
      ? {
          wrapperCol: { span: 14, offset: 4 },
        }
      : null;

  return (
    <>
      <Form
        {...formItemLayout}
        layout={formLayout}
        form={form}
        initialValues={{ layout: formLayout }}
        onValuesChange={onFormLayoutChange}
      >
        {/* <Form.Item label="Form Layout" name="layout">
          <Radio.Group value={formLayout}>
            <Radio.Button value="horizontal">Horizontal</Radio.Button>
            <Radio.Button value="vertical">Vertical</Radio.Button>
            <Radio.Button value="inline">Inline</Radio.Button>
          </Radio.Group>
        </Form.Item> */}
        <Form.Item label="ENG:" className={s.input_section}>
          <Input placeholder="Слово на английском" />
        </Form.Item>
        <Form.Item label="RU:" className={s.input_section}>
          <Input placeholder="Перевод" />
        </Form.Item>
        <Form.Item {...buttonItemLayout}>
          <Button type="primary">Добавить</Button>
        </Form.Item>
      </Form>
    </>
  );
};

export default FormInput;