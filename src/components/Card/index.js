import React from 'react'
import cl from 'classnames';
import { CheckSquareOutlined, DeleteOutlined } from '@ant-design/icons';
import {withRouter} from 'react-router-dom'
import s from './Card.module.scss';

class Card extends React.Component {
    state = {
        done: false
    }

    componentDidMount() {
        const { match: { params }, index, isRemembered} = this.props;

        if(index === +params.id) {
            this.setState({
                done: params.isDone,
            })
        }

        if(isRemembered) {
            this.setState({
                done: true
            })
        }
    }

    handleCardClick = () => {
        this.setState(({ done, isRemembered }) => {
            if(isRemembered) return;
            return {
                done: !done,
            }
        });
    }

    handleIsRememberClick = () => {
        const { isRemembered } = this.props;
        this.props.handlePostRemember(this.props.id, isRemembered)
        this.setState({
                done: !isRemembered
            });
    }

    handleDeletedClick = () => {
        this.props.onDeleted();
    }

    render() {
        const {eng, rus, isRemembered} = this.props
        const { done } = this.state
        
        return (
            <div className={s.root}>
                <div
                    className={ cl(s.card, { 
                        [s.done]: done,
                        [s.isRemembered]: isRemembered,
                    }) }
                    onClick={ this.handleCardClick}
                >
                    <div className={s.cardInner}>
                        <div className={s.cardFront}>
                            {eng}
                        </div>
                        <div className={s.cardBack}>
                            {rus}
                        </div>
                    </div>
                </div>
                <div className={s.icons}>
                    <CheckSquareOutlined onClick={this.handleIsRememberClick}/>
                </div>
                <div className={cl(s.icons, s.deleted)}>
                    <DeleteOutlined onClick={this.handleDeletedClick} />
                </div>
            </div>
        )
    }

}


export default withRouter(Card);
