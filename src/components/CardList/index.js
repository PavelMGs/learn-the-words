import React, { Component } from 'react';
import FirebaseContext from '../../context/FirebaseContext';
import Card from '../Card';

import s from './CardList.module.scss';

class CardList extends Component {
    state = {
        value: '',
        label: '',
    }

    handleInputChange = (e) => {
        console.dir(e.target.value);
        this.setState({
            value: e.target.value
        });
    };

    handleSubmitForm = (e) => {
        e.preventDefault();
        this.setState(({value}) => {
            return {
                label: value,
                value: '',
            }
        });
    };

    render() {
        const { items = [], onDeletedItem, handlePostRemember } = this.props;

        return (
            <>
            <div>
                { this.state.label }
            </div>
                
                <div className={s.root}>
                    {
                        items.map(({ eng, rus, id, isRemembered }, index) => (
                            
                            <Card 
                                onDeleted = {() => {
                                    onDeletedItem(id);
                                }}
                                key={id}
                                eng={eng}
                                rus={rus}
                                index={index}
                                id={id}
                                isRemembered={isRemembered}
                                handlePostRemember={handlePostRemember}
                            />
                        ))
                    }
                </div>
            </>
        );
    }
}

CardList.contextType= FirebaseContext;

export default CardList;
