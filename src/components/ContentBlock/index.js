import React from 'react'
import s from './ContentBlock.module.css'

const ContentBlock = ({ title, descr }) => {
    return (
        <div className = {s.cover}>
            <div className = {s.wrap}>
                {title && <h1 className={s.header}>{title}</h1>}
                {descr && <p className={s.descr}>{descr}</p>}
            </div>
        </div>
    )
};

export default ContentBlock;