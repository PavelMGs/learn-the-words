import * as firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyDiVh74DyNi0YNjGoBhYegI2SijNxnkmtU",
    authDomain: "learn-the-words-ce8bb.firebaseapp.com",
    databaseURL: "https://learn-the-words-ce8bb.firebaseio.com",
    projectId: "learn-the-words-ce8bb",
    storageBucket: "learn-the-words-ce8bb.appspot.com",
    messagingSenderId: "586342549831",
    appId: "1:586342549831:web:01e26d715ce278c86b6462"
  };
  
class Firebase {
  constructor() {
    firebase.initializeApp(firebaseConfig);

    this.auth = firebase.auth();
    this.database = firebase.database();

    this.userUid = null;
  }

  setUserUid = (uid) => this.userUid = uid;

  signWithEmail = (email, password) => this.auth.signInWithEmailAndPassword(email, password);
  createUser = (email, password) => this.auth.createUserWithEmailAndPassword(email, password);
  getUserCardsRef = () => this.database.ref( `/${this.userUid}`);
  getUserCurrentCardRef = (id) => this.database.ref(`/${this.userUid}/${id}`);
}
  
export default Firebase;