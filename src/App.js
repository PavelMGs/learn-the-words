import React, { Component } from "react";
import HomePage from './pages/Home/index';
import LoginPage from './pages/Login';
import { Spin, Layout, Menu } from 'antd';
import { Route, Link, Switch, Redirect } from "react-router-dom";
import CurrentCard from './pages/CurrentCard';
import { connect } from 'react-redux';

import s from "./App.module.scss";
import FirebaseContext from "./context/FirebaseContext";
import { PrivateRoute } from "./utils/privateRoute";
import { bindActionCreators } from "redux";
import { addUserAction } from "./action/userAction";
import About from "./pages/About";

const { Header, Content } = Layout;
class App extends Component {

  componentDidMount() {
    const { auth, setUserUid } = this.context;
    const { addUser } = this.props;

    auth.onAuthStateChanged((user) => {
      if (user) {
        setUserUid(user.uid);
        localStorage.setItem('user', JSON.stringify(user.uid));
        addUser(user);
      }
      else {
        setUserUid(null);
        localStorage.removeItem('item');
      }
    });
  };

  handleLogout = () => {
    const { auth } = this.context;
    auth.signOut();
    localStorage.removeItem('user');
  }

  render() {
    const { userUid } = this.props;

    if (userUid === null) {
      return (
        <div className={s.loader_wrap}>
          <Spin size="middle" />
        </div>
      );
    }

    return (
      <>
        <Route path="/login" component={LoginPage} />
        <Route render={(props) => {
          return (
            <Layout
            height='64'>

              <Header style = {{
                  display: props.location.pathname === '/login' ? 'none' : 'block'
                }}>
                <Menu theme="dark" mode="horizontal" className={s.menu}>
                  <Menu.Item key="1">
                    <Link to="/">Home</Link>
                  </Menu.Item>
                  <Menu.Item key="2">
                    <Link to="/about">About</Link>
                  </Menu.Item>
                  <Menu.Item className={s.menu_button}>
                      <button onClick={() => {
                        this.handleLogout()
                        props.history.push('/login')
                        }} className={s.btn}>
                        Logout
                      </button>
                  </Menu.Item>
                </Menu>
              </Header>

              <Content>
                <Switch>
                  <PrivateRoute path="/login" component={LoginPage} />
                  <PrivateRoute path="/" exact component={HomePage} />
                  <PrivateRoute path="/home/:id?/:isDone?" component={HomePage} />
                  <PrivateRoute path="/about" component={About} />
                  <Redirect to='/' />
                </Switch>
              </Content>

            </Layout>
          )
        }
        } />
      </>

    );
  }
}

App.contextType = FirebaseContext;

const mapStateToProps = (state) => {
  return {
    userUid: state.user.userUid
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    addUser: addUserAction,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
